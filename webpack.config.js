"use strict";

const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');

const extractLESS = new ExtractTextPlugin('../../dist/css/main.css');

module.exports = {
    entry: [
        path.resolve(__dirname, 'src/less') + '/index.less',
        path.resolve(__dirname, 'src/js') + '/index.js',
    ],
    output: {
        path: __dirname + '/dist/js',
        filename: 'bundle.js',
        },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                exclude: /node_modules/,
                test: /\.js$/,
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.less$/,
                use: extractLESS.extract(['css-loader', 'less-loader'])
            },
        ]
    },
    plugins: [
        extractLESS
    ],
}
