export const currencies = {
  PLN: {
    symbol: "zł",
    name: "Polski Złoty"
  },
  USD: {
    symbol: "&#36;",
    name: "United States Dollar"
  },
  AUD: {
    symbol: "&#36;",
    name: "Australia Dollar"
  },
  CAD: {
    symbol: "&#36;",
    name: "Canada Dollar"
  },
  EUR: {
    symbol: "&#8364;",
    name: "Euro Member Countries"
  },
  HUF: {
    symbol: "&#70;",
    name: "Hungary Forint"
  },
  CHF: {
    symbol: "&#67;",
    name: "Switzerland Franc"
  },
  GBP: {
    symbol: "&#163;",
    name: "United Kingdom Pound"
  },
  JPY: {
    symbol: "&#165;",
    name: "Japan Yen"
  },
  CZK: {
    symbol: "&#75;",
    name: "Czech Republic Koruna"
  },
  DKK: {
    symbol: "&#107;",
    name: "Denmark Krone"
  },
  NOK: {
    symbol: "&#107;",
    name: "Norway Krone"
  },
  SEK: {
    symbol: "&#107;",
    name: "Sweden Krona	"
  },
  XDR: {
    symbol: "XDR",
    name: "Special drawing rights"
  }
};