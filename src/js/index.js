"use strict";
import {currencies} from './../data';

console.log(currencies);

const button = document.getElementById("submit");
const input = document.getElementById("inputValue");
const output = document.getElementById("result");
const advancedSelectBox1 = document.getElementsByClassName("advancedSelect")[0];
const advancedSelectBox2 = document.getElementsByClassName("advancedSelect")[1];
const listElements1 = document.getElementsByClassName("dropdown")[0];
const listElements2 = document.getElementsByClassName("dropdown")[1];

document.addEventListener("click", closeList);
button.addEventListener("click", justDoIt);
input.addEventListener("keydown", keyDown);
advancedSelectBox1.addEventListener("click", selectBoxHandler);
advancedSelectBox2.addEventListener("click", selectBoxHandler);
listElements1.addEventListener("click", currencySymbol);
listElements2.addEventListener("click", currencySymbol);


function currencySymbol(e) {
    if (e.target && e.target.nodeName == "LI") {
        let currency = e.target.innerHTML;
        this.parentElement.getElementsByClassName("defaultValue")[0].innerHTML = currency;
    }
}

function closeList(e) {
    let dropdown1 = this.getElementsByTagName("UL")[0];
    let dropdown2 = this.getElementsByTagName("UL")[1];
    let arrow1 = this.getElementsByClassName("arrow")[0];
    let arrow2 = this.getElementsByClassName("arrow")[1];

    if (e.target != document.getElementsByClassName('selectHeader')[0] &&
        e.target != document.getElementsByClassName('selectHeader')[1] &&
        e.target != document.getElementsByClassName('defaultValue')[0] &&
        e.target != document.getElementsByClassName('defaultValue')[1]) {

        if(dropdown1.classList.contains("show")) {
            dropdown1.classList.remove("show");
            arrow1.classList.add("collapsed");
        }
        if(dropdown2.classList.contains("show")) {
            dropdown2.classList.remove("show");
            arrow2.classList.add("collapsed");
        }
    }
}

function selectBoxHandler() {
    let currencySymbol = this.getElementsByTagName("UL")[0];
    let arrowSymbol = this.getElementsByClassName("arrow")[0];

    if (currencySymbol.classList.contains("show")) {
        currencySymbol.classList.remove("show");
        arrowSymbol.classList.add("collapsed");
    }
    else {
        currencySymbol.classList.add("show");
        arrowSymbol.classList.remove("collapsed");
    }
}

function getValue() {
    return parseFloat(input.value) ? parseFloat(input.value) : false;
}

function printValue(calculationResult, currencyData) {
    output.innerHTML = `${calculationResult} ${currencyData.symbol}`;
    document.styleSheets[0].addRule('#result::after', `content: '${currencyData.name}'`);
}

function keyDown(e) {
    if (e.keyCode === 13) {
        justDoIt();
    }
}


function justDoIt() {
    let val = getValue();
    let result = document.getElementsByClassName("currencyResult")[0];

    if (val) {
        let firstCurrencyCode = getFirstValue();
        let secondCurrencyCode = getSecondValue();

        //zwraca promis
        let data = getData(firstCurrencyCode, secondCurrencyCode);

        data.then(res => {
            let currencyData = currencies[secondCurrencyCode];
            let result = calculateResult(val, res.converter1, res.converter2);
            console.log(result);
            console.log(currencyData);
            printValue(result.toFixed(2), currencyData);
        });
        result.classList.add("showResult");
    }
    else {
        printValue("");
        result.classList.remove("showResult");
        return;
    }
}

function getFirstValue() {
    const getValue = document.getElementById("defaultValue1").innerHTML;
    return getValue;

}

function getSecondValue() {
    const getValue = document.getElementById("defaultValue2").innerHTML;
    return getValue;
}

function getData(firstCode, secondCode) {
    if (firstCode === "PLN" && secondCode === "PLN") {
        return new Promise((resolve) => {
            alert("Wybrałeś te same waluty. Wybierz inną.");
            resolve({
                converter1: 1,
                converter2: 1
            })
        })
    }
    else if (firstCode === "PLN") {
        return fetch(`http://api.nbp.pl/api/exchangerates/rates/c/${secondCode}/`)
            .then(response => response.json())
            .then(data => {
                return {
                    converter1: 1,
                    converter2: data.rates[0].bid
                };
            })
            .catch(error => error);
    }
    else if (secondCode === "PLN") {
        return fetch(`http://api.nbp.pl/api/exchangerates/rates/c/${firstCode}/`)
            .then(response => response.json())
            .then(data => {
                return {
                    converter1: data.rates[0].ask,
                    converter2: 1
                };
            })
            .catch(error => error);
    }
    else {
        return fetch(`http://api.nbp.pl/api/exchangerates/rates/c/${firstCode}/`)
            .then(firstResponse => firstResponse.json())
            .then(firstData => fetch(`http://api.nbp.pl/api/exchangerates/rates/c/${secondCode}/`)
                .then(secondResponse => secondResponse.json())
                .then(secondData => {
                    return {
                        converter1: firstData.rates[0].ask,
                        converter2: secondData.rates[0].bid
                    };
                })
            )
            .then(data => data)
            .catch(error => error);
    }

}

function calculateResult(inputValue, firstValue, secondValue) {
    return inputValue * firstValue / secondValue;
}

